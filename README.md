[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/mf-lcd)](https://api.reuse.software/info/codeberg.org/infrared/mf-lcd)

This README is part of the mf-lcd project.

SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

RP2040 forth driver for HD44780U-compatible LCD character displays. Written for Mecrisp Stellaris Forth.

# Dependencies

- RP2040-based platform. I use the Rpi Pico.
- Mecrisp Stellaris Forth version 2.6.3: specifically, the "mecrisp-stellaris-pico-with-tools.uf2" build for rp2040.
- See file "lcd.fs" for information regarding the LCD character display connections

# Modules

Please see each module file for additional documentation.

Modules should be loaded in the following order:

- marker.fs
- dictionary.fs
- lcd.fs
- lcd-demo.fs (optional)

lcd-demo contains a few demo words designed for 20x4 character displays.
