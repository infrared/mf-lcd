\ ******************************************************************
\ lcd.fs is part of the mf-lcd project
\
\ SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ RP2040 forth driver for HD44780U-compatible LCD character displays.
\ Written for Mecrisp Stellaris Forth.
\
\ Provides primitive words to initialize the display and to display
\ characters.
\
\ Tested on a Sunfounder LCD2004 module with a Rpi Pico board.
\ Makes the following assumptions:
\ - Four-bit wiring: D4-D7, enable pin, and RS pin only
\ - R/W pin is grounded (write only)
\ - Adjust the pin assignment constants below as desired
\ - Adjust the default-mode constants below as desired. Note that my
\   test LCD is a a 20x4 display, with only a 5x8 pixel character
\   display, so I did not test single-line mode or 5x10 font mode.
\   My test LCD uses two-line mode, with first logical line mapping
\   to the first and third physical lines, and the second logical
\   line mapping to the second and fourth physical lines.
\
\ Call `init-lcd' to get the display setup for the above
\ configuration. This will also clear the screen. Do not run
\ init-lcd again unless the display has been fully powered-off.
\
\ public:
\   clear-display display-set init-lcd return-home set-cgram-addr
\   set-ddram-addr write-to-ram
\
\ example (prints `hi' on the screen):
\  init-lcd  ok.
\  char h write-to-ram  ok.
\  char i write-to-ram  ok.
\ ******************************************************************

lcd
marker lcd

\ ******************************************************************
\ SIO addresses for GPIO operations
\ ******************************************************************
$d0000000 constant SIO_BASE
SIO_BASE $004 + constant GPIO_IN       \ Input value for GPIO pins
SIO_BASE $010 + constant GPIO_OUT      \ GPIO output value
SIO_BASE $014 + constant GPIO_OUT_SET  \ GPIO output value set
SIO_BASE $018 + constant GPIO_OUT_CLR  \ GPIO output value clear
SIO_BASE $01c + constant GPIO_OUT_XOR  \ GPIO output value XOR
SIO_BASE $020 + constant GPIO_OE       \ GPIO output enable
SIO_BASE $024 + constant GPIO_OE_SET   \ GPIO output enable set
SIO_BASE $028 + constant GPIO_OE_CLR   \ GPIO output enable clear
SIO_BASE $02c + constant GPIO_OE_XOR   \ GPIO output enable XOR


\ ******************************************************************
\ Pin assigments
\ ******************************************************************
2 constant RS_PIN
3 constant E_PIN
4 constant D4_PIN
5 constant D5_PIN
6 constant D6_PIN
7 constant D7_PIN

\ ******************************************************************
\ Timing constants - trying to follow the HD44780U data sheet
\                    with ns values rounded up to 1 us
\ tfact1: with my test equipment, the rise/fall/setup time had to be
\         quite a bit larger than what was listed in the data sheet.
\         Extra inductance...? GPIO capacitance...?
\ ******************************************************************
20 constant tfact1
1 tfact1 * constant t_er \ enable rise
1 tfact1 * constant t_ef \ enable fall
1 tfact1 * constant t_as \ addr set up (RS -> E)
1 tfact1 * constant t_dsw \ data set up
37 constant t_iscg \ set cgram instruction
37 constant t_isdd \ set ddram instruction
40 constant t_iwtr \ write data to cg or ddram
4000 constant t_icd \ clear display
1520 constant t_irh \ return home
37 constant t_ifs \ function set
37 constant t_ido \ display on/off
37 constant t_iems \ entry mode set

\ ******************************************************************
\ Default mode constants (used during init-lcd)
\ ******************************************************************
true constant d_display_on
true constant d_cursor_on
false constant d_blink_on
\ 0 = single-line mode, 1 = two-line mode:
1 constant d_line_mode
\ 0 = 5x8 font mode, 1 = 5x10 font mode
0 constant d_font_mode

: set-d-pins ( nibble -- )
    dup 1 D7_PIN lshift swap %1000 and if GPIO_OUT_SET ! else GPIO_OUT_CLR ! then
    dup 1 D6_PIN lshift swap %100 and if GPIO_OUT_SET ! else GPIO_OUT_CLR ! then
    dup 1 D5_PIN lshift swap %10 and if GPIO_OUT_SET ! else GPIO_OUT_CLR ! then
    1 D4_PIN lshift swap %1 and if GPIO_OUT_SET ! else GPIO_OUT_CLR ! then
    t_dsw us ;

: enable-up ( -- ) 1 E_PIN lshift GPIO_OUT_SET ! t_er us ;

: enable-down ( -- ) 1 E_PIN lshift GPIO_OUT_CLR ! t_ef us ;

: rs-up ( -- ) 1 RS_PIN lshift GPIO_OUT_SET ! t_as us ;

: rs-down ( -- ) 1 RS_PIN lshift GPIO_OUT_CLR ! t_as us ;

\ ******************************************************************
\ Sets the CGRAM address to `byte', and cause subsequent RAM writes
\ to write to CGRAM. Only the 6 lowest bits of the input byte are
\ used.
\ ******************************************************************
: set-cgram-addr ( byte -- )
    rs-down
    enable-up
    dup 4 rshift %0100 or %0111 and set-d-pins t_dsw us
    enable-down
    enable-up
    %1111 and set-d-pins
    enable-down
    t_iscg us
;

\ ******************************************************************
\ Sets the DDRAM address to `byte', and cause subsequent RAM writes
\ to write to DDRAM. Only the 7 lowest bits of the input byte are
\ used.
\ ******************************************************************
: set-ddram-addr ( byte -- )
    rs-down
    enable-up
    dup 4 rshift %1000 or set-d-pins t_dsw us
    enable-down
    enable-up
    %1111 and set-d-pins
    enable-down
    t_isdd us
;

\ ******************************************************************
\ Writes a byte either to DDRAM or CGRAM.
\ ******************************************************************
: write-to-ram ( byte -- )
    rs-up
    enable-up
    dup 4 rshift set-d-pins
    enable-down
    enable-up
    %1111 and set-d-pins
    enable-down
    t_iwtr us
;

: clear-display
    rs-down
    enable-up
    %0000 set-d-pins
    enable-down
    enable-up
    %0001 set-d-pins
    enable-down
    t_icd us
;

: return-home ( -- )
    rs-down
    enable-up
    %0000 set-d-pins
    enable-down
    enable-up
    %0010 set-d-pins
    enable-down
    t_irh us
;

\ ******************************************************************
\ Arguments:
\   display on/off
\   cursor on/off
\   blinking cursor position on/off
\ ******************************************************************
: display-set ( bool bool bool -- )
    %1000 swap if 1 or then
    swap if %10 or then
    swap if %100 or then
    rs-down
    enable-up
    %0000 set-d-pins
    enable-down
    enable-up
    set-d-pins
    enable-down
    t_ido us
;

\ ******************************************************************
\ Init'ing the LCD twice causes screen to seem to freeze up. This is
\ due to nibbles getting out of order due to the call to an 8-bit
\ operation after already being in 4-bit mode. However, calling
\ init-lcd again "unfreezes" the display and clears the screen.
\ ******************************************************************
: init-lcd ( -- )
    \ ******************************************************************
    \ Prep pins as output pins
    \ ******************************************************************
    1 RS_PIN lshift
    1 E_PIN lshift or
    1 D4_PIN lshift or
    1 D5_PIN lshift or
    1 D6_PIN lshift or
    1 D7_PIN lshift or GPIO_OE !
    \ ******************************************************************
    \ enable 4-bit operation
    \ ******************************************************************
    rs-down
    enable-up
    %0010 set-d-pins
    enable-down
    \ ******************************************************************
    \ function set, repeated 4-bit part
    \ ******************************************************************
    enable-up
    %0010 set-d-pins
    enable-down
    \ ******************************************************************
    \ function set, 2-line display & 5x8 dot charset
    \ ******************************************************************
    enable-up
    d_line_mode 3 lshift d_font_mode 2 lshift or set-d-pins
    enable-down
    t_ifs us
    \ ******************************************************************
    \ display set
    \ ******************************************************************
    d_display_on d_cursor_on d_blink_on display-set
    \ ******************************************************************
    \ entry mode set
    \ ******************************************************************
    enable-up
    %0000 set-d-pins
    enable-down
    enable-up
    %0110 set-d-pins
    enable-down
    t_iems us
    clear-display
;
