\ ******************************************************************
\ marker.fs part of the mf-lcd project
\
\ This module provides the `marker' word.
\
\ SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
\ SPDX-FileCopyrightText: 2022 Matthias Koch <m.cook@gmx.net>
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ Matthias Koch wrote this code originally but gave me (Christopher
\ Howard) permission to use it in my project.
\
\ public words:
\   marker
\ ******************************************************************

\ ******************************************************************
\ Special for RP2040, works only if everything is in RAM memory.
\ ******************************************************************
: marker ( -- ) \ marker <name> --
  \ Save current state of the dictionary
  (dp) @ (latest) @ compiletoram?
  <builds , , ,
  \ Rewind dictionary
  does>
  here >r \ To clear the area from "flash" memory
  dup           @ if compiletoram else
compiletoflash then
  dup 1 cells + @ (latest) !
      2 cells + @ (dp) !
  \ When in compiletoflash mode, clear link field of latest
  \ definition and the space freed
  compiletoram? not
  if
    -1 (latest) @ !
    here r> over - $FF fill
  else rdrop then
;
