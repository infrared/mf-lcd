marker dictionary

\ ******************************************************************
\ dictionary.fs part of the mf-lcd project
\
\ This module provides dictionary management words.
\
\ SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ public words:
\   behead
\ ******************************************************************

\ ******************************************************************
\ code>link lifted from dictionary-tools.txt in
\ mecrisp-stellaris-2.6.3
\ ******************************************************************
: link>code  ( addr -- addr* ) 6 + skipstring ;
0 variable searching-for
0 variable closest-found
\ Try to find this code start address in dictionary
: code>link  ( entrypoint -- addr | 0 )
    searching-for !
    0 closest-found !
    \ Save current compile mode
    compiletoram? 0= >r
    \ Always scan in compiletoram mode, in order to also find
    \ definitions in RAM.
    compiletoram
    dictionarystart
    begin
        dup link>code searching-for @ = if dup closest-found ! then
        dictionarynext
    until
    drop
    r> if compiletoflash then \ Restore compile mode
    closest-found @
;


\ ******************************************************************
\ FIND-LINK ( a-addr -- a-addr flag )
\   a-addr: link value you are looking for
\   a-addr: address of dict entry having that link value
\   flag: indicates success or failure
\ ******************************************************************
: find-link
    dictionarystart
    begin
        2dup @ dup $ffffffff = if 2drop 2drop 0 0 true
        else = if nip 1 true
            else @ false then then
    until ;

\ ******************************************************************
\ BEHEAD <name> ( -- )
\   Hide the dictionary entry for <name>, without removing it.
\   This allows you to remove a word from the wordlist, without
\   breaking code that used it.
\ ******************************************************************
: behead
    token find drop dup 0 =
    if drop drop ." could not find word"
    else
        code>link dup @ swap find-link
        if ! else drop ." find-link failed" then
    then ;

behead link>code
behead searching-for
behead closest-found
behead code>link
behead find-link
