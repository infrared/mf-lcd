\ ******************************************************************
\ lcd-demo.fs is part of the mf-lcd project
\
\ SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ All demos assume the following:
\ - you have already run `init-lcd'
\ - 20x4 character, 5x8 pixel display
\
\ dependencies:
\   marker.fs dictionary.fs lcd.fs
\
\ public words:
\   demo-characters demo-sine
\ ******************************************************************

lcd-demo
marker lcd-demo

: print-16-patterns ( start-code -- )
    dup $f0 and #4 rshift dup $a < if $30 + else $37 + then write-to-ram
    dup $f and dup $a < if $30 + else $37 + then write-to-ram
    $20 write-to-ram
    dup #16 + swap do i write-to-ram loop ;

\ ******************************************************************
\ Display all 256 characters, using four pages with four lines each.
\ Waits until a key is received on the serial line to display the
\ next page.
\ ******************************************************************
: demo-characters ( -- )
    true false false display-set
    #256 0 do
        clear-display
        0 set-ddram-addr i print-16-patterns
        $40 set-ddram-addr i #16 + print-16-patterns
        #20 set-ddram-addr i #32 + print-16-patterns
        $40 #20 + set-ddram-addr i #48 + print-16-patterns
        begin 100 ms key? until key
    64 +loop
    clear-display
    true true false display-set ;
 

: wr write-to-ram ;

: store-bitmap-bars
    0 set-cgram-addr
    \ 0: OFF OFF OFF
    8 0 do 0 wr loop
    \ 1: ON  OFF OFF
    2 0 do %11111 wr loop
    6 0 do 0 wr loop
    \ 2: OFF ON  OFF
    3 0 do 0 wr loop
    2 0 do %11111 wr loop
    3 0 do 0 wr loop
    \ 3: OFF OFF ON
    6 0 do 0 wr loop
    2 0 do %11111 wr loop
    \ 4: ON  ON  OFF
    2 0 do %11111 wr loop
    0 wr
    2 0 do %11111 wr loop
    3 0 do 0 wr loop
    \ 5: ON  OFF ON
    2 0 do %11111 wr loop
    0 wr
    3 0 do 0 wr loop
    2 0 do %11111 wr loop
    \ 6: OFF ON  ON
    3 0 do 0 wr loop
    2 0 do %11111 wr loop
    0 wr
    2 0 do %11111 wr loop
    \ 7: ON  ON  ON
    2 0 do %11111 wr loop
    0 wr
    2 0 do %11111 wr loop
    0 wr
    2 0 do %11111 wr loop ;

\ ******************************************************************
\ Draws a low-resolution picture of a sine wave on the display,
\ using fake bitmap-like graphics. This is achieved using horizontal
\ bar patterns loaded into the eight CGRAM characters.
\ ******************************************************************
: demo-sine
    store-bitmap-bars
    clear-display
    true false false display-set
    \ first row
    0 wr 0 wr 3 wr 6 wr 7 wr 7 wr 7 wr 6 wr 3 wr 11 0 do 0 wr loop
    \ third row
    11 0 do 7 wr loop 3 wr 7 0 do 0 wr loop 3 wr
    \ second row
    0 wr 6 wr 7 0 do 7 wr loop 6 wr 10 0 do 0 wr loop
    \ fourth row
    12 0 do 7 wr loop 6 wr 3 wr 0 wr 0 wr 0 wr 3 wr 6 wr 7 wr
    begin 100 ms key? until key
    clear-display
    true true false display-set ;

behead print-16-patterns
behead wr
behead store-bitmap-bars
